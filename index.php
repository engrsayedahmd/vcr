<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/22/18
 * Time: 8:34 AM
 */


require_once 'app/inc/session.php';

if(isset($_SESSION['auth']))
{
	header("location: home.php");
}
else
{
	header('Location: login.php');
}