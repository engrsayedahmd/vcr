<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/22/18
 * Time: 6:55 AM
 */

require_once '../app/inc/session.php';

unset($_SESSION['auth']);

session_destroy();

if(!isset($_SESSION['auth']))
{
    header('Location: login.php');
}