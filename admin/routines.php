<?php

require_once '../app/routine-controller.php';
require_once '../app/batch-controller.php';
require_once '../app/faculty-controller.php';

$routines_all = routines_all();
$batches = batches_all();
$faculties = faculty_all();

if(isset($_POST['submit']))
{
    if((add_routine() == true))
    {
        header('Location: routines.php');
    }
}

?>

<?php require('header.php'); ?>
<section id="routin">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="routine-txt">
                        <div class="card">
                            <div class="card-header">
                                Add Routine
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <form action="routines.php" method="post" enctype="multipart/form-data">
                                        <table class="table" id="tb">
                                            <thead>

                                            <a href="#" id="addMore" type="button" class="btn btn-default" title="Add Row">Add Row</a>

                                            <tr>
                                                <td>
                                                    <div class="form-row">
                                                        <div class="form-group col-lg-6">
                                                            <label for="batch">Batch</label>
                                                            <select name="batch" class="form-control" id="batch">
                                                                <option value="">Select</option>
                                                                <?php foreach ($batches as $batch):?>
                                                                    <option value="<?= $batch['batch_name']?>"><?= $batch['batch_name'] ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-lg-6">
                                                            <label for="day">Day</label>
                                                            <select name="day" class="form-control" id="day">
                                                                <option value="">Select</option>
                                                                <option value="Sat">Sat</option>
                                                                <option value="Sun">Sun</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-row">

                                                        <div class="form-group col-lg-6">
                                                            <div class="form-group">
                                                                <label for="subject_name">Subject</label>
                                                                <input type="text" name="subject_name[]" class="form-control" id="subject_name" placeholder="Subject Name">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-6">
                                                            <div class="form-group">
                                                                <label for="subject_code">Code</label>
                                                                <input type="text" name="subject_code[]" class="form-control" id="subject_code" placeholder="Subject Code">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-4">
                                                            <div class="form-group">
                                                                <label for="f_initial">Faculty Initial</label>
                                                                <select name="f_initial[]" class="form-control" id="f_initial">
                                                                    <option value="">Select</option>
                                                                    <?php foreach ($faculties as $faculty):?>
                                                                        <option value="<?= $faculty['initial']?>"><?= $faculty['initial']?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-4">
                                                            <div class="form-group">
                                                                <label for="start_time">Start Time</label>
                                                                <input type="time"  name="start_time[]" class="form-control" id="start_time">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-4">
                                                            <div class="form-group">
                                                                <label for="end_time">End Time</label>
                                                                <input type="time" name="end_time[]" class="form-control" id="end_time">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td>
                                                    <div class="form-row">
                                                        <div class="form-group col-lg-12">
                                                            <div class="form-group">
                                                                <input type="submit" name="submit" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7">
                    <div class="routine-txt">
                        <div class="card">
                            <div class="card-header">Overview</div>
                            <div class="card-body">
                                <table class="table table-bordered table-responsive">
                                    <tr>
                                        <th>Batch</th>
                                        <th>Day</th>
                                        <th>Subject</th>
                                        <th>Code</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Teacher</th>
                                    </tr>
                                    <?php foreach ($routines_all as $routine): ?>
                                        <tr>
                                            <td><?= $routine['batch_name'] ?></td>
                                            <td><?= $routine['day'] ?></td>
                                            <td><?= $routine['subject'] ?></td>
                                            <td><?= $routine['code'] ?></td>
                                            <td><?= $routine['start_time'] ?></td>
                                            <td><?= $routine['end_time'] ?></td>
                                            <td><?= $routine['f_initial'] ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>

<script>
    $(function(){
        $('#addMore').click(function (event) {
            event.preventDefault();
        })
    });
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>




<script>

    var jQuery1111 = jQuery.noConflict();

    $(function(){
        jQuery1111('#addMore').on('click', function() {
            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
            data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
