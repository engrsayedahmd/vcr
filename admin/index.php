<?php

require_once '../app/inc/session.php';

if(isset($_SESSION['admin']))
{
	header('Location: dashboard.php');
}
else
{
	header('Location: login.php');
}