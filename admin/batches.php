<?php

require_once '../app/batch-controller.php';

$batches = batches_all();

if(isset($_POST['submit']))
{
    if(add_batch() == true)
    {
        header('Location: batches.php');
    }
}
?>

<?php require('header.php'); ?>
<section id="routin">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Create Batch
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <form action="batches.php" method="post" enctype="multipart/form-data">
                                        <div class="form-row">
                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <label for="batch_name">Batch Name</label>
                                                    <input type="text" name="batch_name" class="form-control" id="batch_name" placeholder="Enter Batch Name">
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <input type="submit" name="submit" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Batches
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <?php foreach ($batches as $batch): ?>
                                    <div class="col-lg-3 text-center" style="">
                                        <div class="batch-single-txt">
                                            <i class="icofont icofont icofont-people"></i>
                                            <h5><?= $batch['batch_name'] ?></h5>
                                            <p>
                                                <strong>Groups: </strong> <span>30</span>
                                            </p>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>




<script>

    var jQuery1111 = jQuery.noConflict();

    $(function(){
        jQuery1111('#addMore').on('click', function() {
            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
            data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
