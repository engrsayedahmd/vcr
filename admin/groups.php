<?php

require_once '../app/group-controller.php';
require_once '../app/batch-controller.php';

$groups = groups_all();
$batches = batches_all();

if(isset($_POST['submit']))
{
    if(add_group() == true)
    {
        header('Location: groups.php');
    }
}
?>

<?php require('header.php'); ?>
<section id="routin">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Create Group
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <form action="groups.php" method="post" enctype="multipart/form-data">
                                        <div class="form-row">

                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <label for="batch_name">Batch Name</label>
                                                    <select name="batch_name" class="form-control" id="batch_name">
                                                        <option value="">Select Batch</option>
                                                        <?php foreach ($batches as $batch):?>
                                                        <option value="<?= $batch['batch_name']?>"><?= $batch['batch_name'] ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <label for="group_name">Group Name</label>
                                                    <input type="text" name="group_name" class="form-control" id="group_name" placeholder="Enter Group Name">
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <input type="submit" name="submit" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Groups
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <?php foreach ($groups as $group): ?>
                                    <div class="col-lg-4 text-center" style="">
                                        <div class="batch-single-txt">
                                            <i class="icofont icofont-group-students"></i>
                                            <h5><?= $group['batch_name'] ?></h5>
                                            <h6>Group: <?= $group['group_name'] ?></h6>
                                            <p>
                                                <strong>Students: </strong> <span>30</span>
                                            </p>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>




<script>

    var jQuery1111 = jQuery.noConflict();

    $(function(){
        jQuery1111('#addMore').on('click', function() {
            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
            data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
