<?php

require_once '../app/batch-controller.php';
require_once '../app/faculty-controller.php';

$batches = batches_all();
$faculties = faculty_all();
$faculty_batches = faculty_batches_all();

if(isset($_POST['submit']))
{
   if(assign_faculty() == true)
   {
        header('Location: assign-faculty.php');
   }
}

?>

<?php require('header.php'); ?>
<section id="routin">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Add Faculty
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <form action="assign-faculty.php" method="post" enctype="multipart/form-data">
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="faculty_id">Faculty Name</label>
                                                    <select name="faculty_id" class="form-control" id="faculty_id">
                                                        <option value="">Select Faculty</option>
                                                        <?php foreach ($faculties as $faculty): ?>
                                                        <option value="<?= $faculty['id']?>"><?= $faculty['name']?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="batch_name">Batch Name</label>
                                                    <select name="batch_name" class="form-control" id="batch_name">
                                                        <option value="">Select Batch</option>
                                                        <?php foreach ($batches as $batch):?>
                                                            <option value="<?= $batch['batch_name']?>"><?= $batch['batch_name'] ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <input type="submit" name="submit" value="Assign" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Faculty
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <table class="table">
                                        <tr>
                                            <th>NAME</th>
                                            <th>INITIAL</th>
                                            <th>BATCH</th>
                                            <th>EDIT</th>
                                            <th>DELETE</th>
                                        </tr>
                                        <?php foreach ($faculty_batches as $faculty): ?>
                                        <tr>
                                            <td><?= $faculty['name'] ?></td>
                                            <td><?= $faculty['initial'] ?></td>
                                            <td><?= $faculty['batch_name'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary">EDIT</button>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger">DELETE</button>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>




<script>

    var jQuery1111 = jQuery.noConflict();

    $(function(){
        jQuery1111('#addMore').on('click', function() {
            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
            data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
