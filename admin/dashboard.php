<?php

require_once '../app/inc/session.php';

?>

<?php require('header.php'); ?>
<section id="dashboard">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Options</div>
                        <div class="card-body">
                            <div class="dash-content">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="dash-single text-center">
                                            <a href="students.php">
                                            <i class="icofont icofont-users-alt-5"></i>
                                            <p>Student Registration</p>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="dash-single text-center">
                                            <a href="faculty.php">
                                                <i class="icofont icofont-users-alt-5"></i>
                                                <p>Faculty Registration</p>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="dash-single text-center">
                                            <a href="assign-faculty.php">
                                                <i class="icofont icofont-users-alt-5"></i>
                                                <p>Assign Faculty</p>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="dash-single text-center">
                                            <a href="assign-cr.php">
                                                <i class="icofont icofont-users-alt-5"></i>
                                                <p>Assign CR</p>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="dash-single text-center">
                                            <a href="batches.php">
                                                <i class="icofont icofont icofont-people"></i>
                                                <p>Batches</p>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="dash-single text-center">
                                            <a href="routines.php">
                                            <i class="icofont icofont-notebook"></i>
                                            <p>Routine</p>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="dash-single text-center">
                                            <a href="notices.php">
                                                <i class="icofont icofont-black-board"></i>
                                                <p>Notice Board</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'Sales', 'Expenses'],
            ['2004', 1000, 400],
            ['2005', 1170, 460],
            ['2006', 660, 1120],
            ['2007', 1030, 540]
        ]);

        var options = {
            title: 'Company Performance',
            curveType: 'function',
            legend: {
                position: 'bottom'
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
    }

</script>
<?php require('footer.php'); ?>
