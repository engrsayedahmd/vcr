<?php

require_once '../app/faculty-controller.php';

if(isset($_POST['submit']))
{
    if(add_faculty() == true)
    {
        header('Location: faculty.php');
    }
}

$faculties = faculty_all();

?>

<?php require('header.php'); ?>
<section id="routin">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Add Faculty
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <form action="faculty.php" method="post" enctype="multipart/form-data">
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="faculty_name">Faculty Name</label>
                                                    <input type="text" name="faculty_name" class="form-control" id="faculty_name" placeholder="Enter Faculty Name">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="faculty_initial">Faculty Initial</label>
                                                    <input type="text" name="faculty_initial" class="form-control" id="faculty_initial" placeholder="Enter Faculty Initial">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="faculty_phone">Faculty Contact No</label>
                                                    <input type="text" name="faculty_phone" class="form-control" id="faculty_phone" placeholder="Enter Faculty Contact No">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="faculty_email">Faculty Email</label>
                                                    <input type="text" name="faculty_email" class="form-control" id="faculty_email" placeholder="Enter Faculty Email">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <input type="submit" name="submit" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Faculty
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>NAME</th>
                                            <th>INITIAL</th>
                                            <th>EMAIL</th>
                                            <th>PHONE</th>
                                            <th>EDIT</th>
                                            <th>DELETE</th>
                                        </tr>
                                        <?php foreach ($faculties as $faculty): ?>
                                        <tr>
                                            <td><?= $faculty['name'] ?></td>
                                            <td><?= $faculty['initial'] ?></td>
                                            <td><?= $faculty['email'] ?></td>
                                            <td><?= $faculty['phone'] ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>




<script>

    var jQuery1111 = jQuery.noConflict();

    $(function(){
        jQuery1111('#addMore').on('click', function() {
            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
            data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
