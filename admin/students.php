<?php

require_once '../app/batch-controller.php';
require_once '../app/students-controller.php';

$batches = batches_all();
$students= students_all();

if(isset($_POST['submit']))
{
    if(add_student() == true)
    {
        header('Location: students.php');
    }
}

?>

<?php require('header.php'); ?>
<section id="routin">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Add Student
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <form action="students.php" method="post" enctype="multipart/form-data">
                                        <div class="form-row">

                                           <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="student_batch">Student Batch</label>
                                                    <select name="student_batch" class="form-control" id="student_batch">
                                                        <option value="">Select Batch</option>
                                                        <?php foreach ($batches as $batch): ?>
                                                            <option value="<?= $batch['batch_name']?>"><?= $batch['batch_name']?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <div class="form-group">
                                                    <label for="student_id">Student ID</label>
                                                    <input type="text" name="student_id" class="form-control" id="student_id" placeholder="Enter Student ID">
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <label for="student_name">Student Name</label>
                                                    <input type="text" name="student_name" class="form-control" id="student_name" placeholder="Enter Student Name">
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <input type="submit" name="submit" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Students
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <table class="table table-bordered table-responsive">
                                        <tr>
                                            <th>ID</th>
                                            <th>NAME</th>
                                            <th>BATCH</th>
                                            <th>EDIT</th>
                                            <th>DELETE</th>
                                        </tr>
                                        <?php foreach ($students as $student): ?>
                                        <tr>
                                            <td><?= $student['student_id'] ?></td>
                                            <td><?= $student['student_name'] ?></td>
                                            <td><?= $student['student_batch'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary">EDIT</button>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger">DELETE</button>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>




<script>

    var jQuery1111 = jQuery.noConflict();

    $(function(){
        jQuery1111('#addMore').on('click', function() {
            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
            data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
