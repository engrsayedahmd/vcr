<?php

require_once '../app/students-controller.php';

if(isset($_POST['submit']))
{
   if(assign_cr() == true)
   {
        header('Location: assign-cr.php');
   }
}

$crs = cr_all();
?>

<?php require('header.php'); ?>
<section id="routin">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                Assign CR
                            </div>
                            <div class="card-body">
                                <div class="table-responsive-lg">
                                    <form action="assign-cr.php" method="post" enctype="multipart/form-data">
                                        <div class="form-row">
                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <label for="student_id">Student ID</label>
                                                    <input type="text" name="student_id" id="student_id" class="form-control" placeholder="Student ID">
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <div class="form-group">
                                                    <input type="submit" name="submit" value="Assign" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7">
                    <div class="batchs-content">
                        <div class="card">
                            <div class="card-header">
                                CR
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-responsive-lg">
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>BATCH</th>
                                        <th>ACTION</th>
                                    </tr>
                                    <?php foreach ($crs as $cr): ?>
                                        <tr>
                                            <td><?= $cr['student_id'] ?></td>
                                            <td><?= $cr['student_name'] ?></td>
                                            <td><?= $cr['student_batch'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary">REMOVE</button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>




<script>

    var jQuery1111 = jQuery.noConflict();

    $(function(){
        jQuery1111('#addMore').on('click', function() {
            var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
            data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
