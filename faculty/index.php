<?php

require_once '../app/inc/session.php';

if(isset($_SESSION['faculty']))
{
	header('Location: home.php');
}
else
{
	header('Location: login.php');
}