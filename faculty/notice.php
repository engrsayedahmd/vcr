<?php

require_once '../app/notice-board-controller.php';
require_once '../app/batch-controller.php';
require_once '../app/security-controller.php';

faculty_access_control();

if(isset($_POST['add_notice']))
{
    if(add_notice('faculty') == true)
    {
        header('Location: notice.php');
    }
}

if(isset($_GET['delete']))
{
    $notice_id = $_GET['delete'];

    if(delete_notice($notice_id) == true)
    {
        header('Location: notice.php');
    }
}

if(isset($_POST['update']))
{
    if(update_notice() == true)
    {
        header('Location: notice.php');
    }
}

$faculty_notices = faculty_notices();
$batches_all = batches_all();

?>

<?php require('header.php'); ?>
<!--    [ Strat Section Area]-->
<section id="notice">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="page-title">
                    <h5>HOME - NOTICE</h5>
                </div>
            </div>
        </div>
        <div class="notice-content">
            <div class="row">
                <div class="col-lg-4">
                    <div class="notice-form">
                        <div class="card">
                            <div class="card-header">
                                <p>Add New Notice</p>
                            </div>
                            <div class="card-body">
                                <form action="notice.php" method="post" enctype="multipart/form-data">
                                    <div class="wrapper-input">
                                        <label for="notice_topic">Notice Topic</label>
                                        <input type="text" name="notice_topic" id="notice_topic" placeholder="Notice Topic">
                                    </div>

                                    <div class="wrapper-input">
                                        <label for="share_with">Share With</label>
                                        <select name="share_with" id="share_with">
                                          <option value="">Select</option>
                                          <option value="Public">Public</option>

                                            <?php foreach ($batches_all as $batch): ?>
                                          <option value="<?= $batch['batch_name']?>"><?= $batch['batch_name']?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>

                                    <div class="wrapper-input">
                                        <label for="notice_body">Notice Body</label>
                                        <textarea name="notice_body" id="notice_body" placeholder="Notice Body"></textarea>
                                    </div>

                                    <div class="wrapper-input">
                                        <label for="file">Upload File (image/pdf/docx)</label>
                                        <input type="file" name="file" id="file">
                                    </div>

                                    <div class="wrapper-input">
                                        <button type="submit" name="add_notice">SUBMIT</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="notice-board">
                        <div class="card">
                            <div class="card-header">
                                <p>Notice Board</p>
                            </div>
                            <div class="card-body">
                                <div class="all-notices">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Title</th>
                                            <th>Body</th>
                                            <th>Files</th>
                                            <th>Shared</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>

                                        <?php foreach ($faculty_notices as $notice):?>
                                        <tr>
                                            <td><?= $notice['notice_topic']?></td>
                                            <td><?= $notice['notice_body']?></td>
                                            <td><?= $notice['uploaded_file']?></td>
                                            <td><?= (!empty($notice['batch_name'])) ? $notice['batch_name'] : 'All'?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit<?= $notice['notice_id']?>">
                                                    Edit
                                                </button>
                                            </td>
                                            <td>
                                                <a href="notice.php?delete=<?= $notice['notice_id']?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                            </td>
                                        </tr>


                                        <!-- Modal -->
                                        <div class="modal fade" id="edit<?= $notice['notice_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <form action="notice.php" method="post" enctype="multipart/form-data">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <div class="notice-form">
                                                                <div class="card">
                                                                    <div class="card-header">
                                                                        <p>Update Notice</p>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="wrapper-input">
                                                                            <label for="notice_topic">Notice Topic</label>
                                                                            <input type="text" name="notice_topic" id="notice_topic" placeholder="Notice Topic" value="<?= $notice['notice_topic']?>">
                                                                            <input type="hidden" name="notice_id" value="<?= $notice['notice_id']?>">
                                                                        </div>

                                                                        <div class="wrapper-input">
                                                                            <label for="share_with">Share With</label>
                                                                            <select name="share_with" id="share_with">

                                                                                <?php if($notice['public'] == true): ?>
                                                                                    <option value="Public">Public</option>
                                                                                <?php else:?>
                                                                                    <option value="<?= $notice['batch_name']?>"><?= $notice['batch_name']?></option>
                                                                                <?php endif; ?>
                                                                                <option value="">Select</option>
                                                                                <option value="Public">Public</option>

                                                                                <?php foreach ($batches_all as $batch): ?>
                                                                                    <option value="<?= $batch['batch_name']?>"><?= $batch['batch_name']?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>

                                                                        <div class="wrapper-input">
                                                                            <label for="notice_body">Notice Body</label>
                                                                            <textarea name="notice_body" id="notice_body" placeholder="Notice Body"><?= $notice['notice_body']?></textarea>
                                                                        </div>

                                                                        <div class="wrapper-input">
                                                                            <label for="file">Upload File (image/pdf/docx)</label>
                                                                            <input type="file" name="file" id="file">
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="submit" name="update" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
