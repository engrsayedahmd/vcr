<?php

require_once 'app/auth-controller.php';

if(isset($_POST['login']))
{
    student_login();
}


?>

<?php require('header.php'); ?>
<style>
    .header-area {
        display: none;
    }

</style>
<section id="update-profile">
    <div class="container">
        <div class="profile-update-from">

            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="atten-id">
                        <div class="atten-logo text-center">
                            <img src="assets/img/logo.png" alt="">
                        </div>
                        <div class="atten-frm">
                            <form action="login.php" method="post" enctype="multipart/form-data">
                                <div class="emply-reg-frm">
                                    <p>Log In</p>
                                    <input type="text" name="student_id" placeholder="Student ID">
                                </div>
                                <div class="emply-reg-frm">

                                    <input type="password" name="password" placeholder="Password">
                                </div>
                                <div class="emply-reg-frm">
                                    <button type="submit" name="login">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <a href="faculty/index.php">Faculty Login</a> | <a href="admin/index.php">Admin Login</a>
        </div>
    </div>
</section>

<?php require('footer.php'); ?>
