<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/21/18
 * Time: 11:24 PM
 */

require_once 'db/db.php';
require_once 'core/UserPermissions.php';

use \user_permissions\UserPermissions as usrperm;

usrperm::db_config($db);

function get_user_perms($student_role)
{
    return usrperm::get_user_perms($student_role);
}
