<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/25/18
 * Time: 11:12 PM
 */

require_once 'db/db.php';
require_once 'core/Batches.php';
require_once 'inc/session.php';

use \batches\Batches as batches;

batches::db_config($db);

function add_batch()
{
    $values = array(
        strtoupper($_POST['batch_name']),
        date('Y-m-d')
    );

    return batches::add_batch($values);
}

function batches_all()
{
    return batches::batches_all();
}
