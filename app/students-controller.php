<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/25/18
 * Time: 11:12 PM
 */

require_once 'db/db.php';
require_once 'core/Students.php';
require_once 'inc/session.php';

use \students\Students as std;

std::db_config($db);

function student_info_by_id($student_id)
{
    return std::student_info_by_id($student_id);
}

function add_student()
{
    $student_id = $_POST['student_id'];
    $student_name = $_POST['student_name'];
    $student_batch = $_POST['student_batch'];
    $password = 'student';
    $student_role = 'student';

    $values = array(
        $student_id,
        $student_name,
        $password,
        $student_role,
        $student_batch,
        date('Y-m-d')
    );

    return std::add_student($values);
}

function students_all()
{
    return std::students_all();
}

function assign_cr()
{
    $student_id = $_POST['student_id'];
    return std::assign_cr($student_id);
}

function cr_all()
{
    return std::cr_all();
}