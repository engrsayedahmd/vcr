<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 5/4/2018
 * Time: 9:23 PM
 */

require_once 'core/Security.php';
require_once 'inc/session.php';

use \security\Security as security;

function faculty_access_control()
{
    security::faculty_access_control();
}

function student_access_control()
{
    security::student_access_control();
}
