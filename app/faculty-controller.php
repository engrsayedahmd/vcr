<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/25/18
 * Time: 11:12 PM
 */

require_once 'db/db.php';
require_once 'core/Faculty.php';
require_once 'inc/session.php';

use \faculty\Faculty as faculty;

faculty::db_config($db);

function add_faculty()
{
    $faculty_name = $_POST['faculty_name'];
    $faculty_initial = $_POST['faculty_initial'];
    $faculty_phone = $_POST['faculty_phone'];
    $faculty_email = $_POST['faculty_email'];
    $password = 'faculty';

    $values = array(
        $faculty_name,
        $faculty_initial,
        $faculty_phone,
        $faculty_email,
        $password,
        date('Y-m-d')
    );

    return faculty::add_faculty($values);
}

function faculty_batches_all()
{
    return faculty::faculty_batches_all();
}

function assign_faculty()
{
    $faculty_id = $_POST['faculty_id'];
    $faculty_batch = $_POST['batch_name'];

    $values = array(
        $faculty_id,
        $faculty_batch
    );

    return faculty::assign_faculty($values);
}

function faculty_all()
{
    return faculty::faculty_all();
}
function faculty_info($faculty_id)
{
    return faculty::faculty_info($faculty_id);
}

function faculty_batches($faculty_id)
{
    return faculty::faculty_batches($faculty_id);
}