<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/21/18
 * Time: 11:50 PM
 */
require_once 'db/db.php';
require_once 'core/Authentication.php';
require_once 'inc/session.php';

use \auth\Authentication as auth;

auth::db_config($db);

function student_login()
{
    $student_id = $_POST['student_id'];
    $password = $_POST['password'];

    return auth::student_login($student_id,$password);
}

function admin_login()
{
	$email = $_POST['email'];
	$password = $_POST['password'];

	return auth::admin_login($email, $password);
}

function admin_info($id)
{
	return auth::admin_info($id);
}

function faculty_login()
{
    $faculty_name = $_POST['faculty_name'];
    $password = $_POST['password'];
    return auth::faculty_login($faculty_name, $password);
}