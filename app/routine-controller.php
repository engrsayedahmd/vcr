<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/25/18
 * Time: 11:12 PM
 */

require_once 'db/db.php';
require_once 'core/Routines.php';
require_once 'inc/session.php';

use \routines\Routines as routines;

routines::db_config($db);

function add_routine()
{
    $count = $_POST['subject_name'];

    for ($i =0; $i < $count; $i++)
    {
        $values = array(
            $_POST['batch'],
            $_POST['day'],
            $_POST['subject_name'][$i],
            $_POST['subject_code'][$i],
            $_POST['f_initial'][$i],
            $_POST['start_time'][$i],
            $_POST['end_time'][$i],
            date('Y-m-d')
        );
        return routines::add_routine($values);
    }


}

function routines_all()
{
    return routines::routines_all();
}

function faculty_routine($batch_name, $f_initial)
{
    return routines::faculty_routine($batch_name, $f_initial);
}

function student_routine($batch_name)
{
    return routines::student_routine($batch_name);
}