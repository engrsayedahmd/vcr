<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/21/18
 * Time: 11:48 PM
 */

namespace auth;


class Authentication {
    private static $db;

    public static function db_config($db_config)
    {
        self::$db = $db_config;
    }

    public static function student_login($student_id,$password)
    {
        $sql = "SELECT * FROM `students` WHERE student_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($student_id));

        if($stmt->rowCount())
        {
            $student = $stmt->fetch(2);

            if($student['password'] == $password)
            {
                $_SESSION['student'] = $student['student_id'];
                $_SESSION['student_name'] = $student['student_name'];
                header('Location: home.php');
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function admin_login($email,$password)
    {
        $sql = "SELECT * FROM `admin` WHERE email=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($email));

        if($stmt->rowCount())
        {
            $admin = $stmt->fetch(2);

            if($admin['password'] == $password)
            {
                $_SESSION['admin'] = $admin['id'];
                $_SESSION['name'] = $admin['name'];
                header('Location: dashboard.php');
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function faculty_login($faculty_name,$password)
    {
        $sql = "SELECT * FROM `faculty` WHERE name = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($faculty_name));

        if($stmt->rowCount())
        {
            $faculty = $stmt->fetch(2);

            if($faculty['password'] == $password)
            {
                $_SESSION['faculty'] = $faculty['id'];
                $_SESSION['user_name'] = $faculty['name'];
                header('Location: home.php');
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function admin_info($id)
    {
        $sql = "SELECT * FROM `admin` WHERE id = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($id));

        return $stmt->fetch(2);
    }
}