<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/27/18
 * Time: 10:24 PM
 */

namespace faculty;


class Faculty {

    private static $db;

    public static function db_config($db_config)
    {
        self::$db = $db_config;
    }

    public static function add_faculty($values)
    {
        $sql = "INSERT INTO faculty (name,initial,phone,email,password,created_on) VALUES(?,?,?,?,?,?)";
        $stmt=self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function faculty_batches($faculty_id)
    {
        $sql = "SELECT faculty.initial,faculty_batches.batch_name FROM faculty RIGHT JOIN faculty_batches ON faculty.id = faculty_batches.faculty_id WHERE faculty.id = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($faculty_id));

        return $stmt->fetchAll(2);
    }

    public static function faculty_batches_all()
    {
        $sql = "SELECT * FROM faculty RIGHT JOIN faculty_batches ON faculty.id = faculty_batches.faculty_id";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }

    public static function assign_faculty($values)
    {
        $sql = "INSERT INTO faculty_batches (faculty_id,batch_name) VALUES (?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function faculty_all()
    {
        $sql = "SELECT * FROM `faculty` ORDER BY id DESC";
        $stmt = self::$db->query($sql);

        return $stmt->fetchAll(2);
    }

    public static function faculty_info($faculty_id)
    {
        $sql = "SELECT * FROM `faculty` WHERE id = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($faculty_id));

        return $stmt->fetch(2);
    }
}