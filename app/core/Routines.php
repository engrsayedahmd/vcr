<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/25/18
 * Time: 11:09 PM
 */

namespace routines;


class Routines {

    private static $db;

    public static function db_config($db_config)
    {
        self::$db = $db_config;
    }

    public static function add_routine($values)
    {
        $sql = "INSERT INTO routine (batch_name,day,subject,code,f_initial,start_time,end_time,creation_date)
                VALUES (?,?,?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function routines_all()
    {
        $sql = "SELECT * FROM `routine` ORDER BY id DESC ";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }

    public static function faculty_routine($batch_name, $f_initial)
    {
        $sql = "SELECT * FROM routine WHERE batch_name =? AND f_initial = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($batch_name,$f_initial));

        return $stmt->fetchAll(2);
    }
    public static function student_routine($batch_name)
    {
        $sql = "SELECT * FROM routine WHERE batch_name=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($batch_name));

        return $stmt->fetchAll(2);
    }

}