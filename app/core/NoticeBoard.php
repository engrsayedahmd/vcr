<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/21/18
 * Time: 1:14 AM
 */

namespace notice_board;


class NoticeBoard {

    private static $db;

    public static function db_config($db_config)
    {
        self::$db = $db_config;
    }

    public static function add_notice($values)
    {
        $sql = "INSERT INTO notice_board (notice_topic,notice_body,uploaded_file,posted_by,poster_type,public,batch_name,posted_on)
                VALUES (?,?,?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function faculty_notices($faculty_name)
    {
        $sql = "SELECT * FROM `notice_board` WHERE posted_by = ? AND poster_type = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($faculty_name, 'faculty'));

        return $stmt->fetchAll(2);
    }

    public static function admin_notices()
    {
        $sql = "SELECT * FROM `notice_board` WHERE poster_type = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('admin'));

        return $stmt->fetchAll(2);
    }

    public static function student_notices($student_batch)
    {
        $sql = "SELECT * FROM `notice_board` WHERE batch_name=? ORDER BY notice_id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($student_batch));

        return $stmt->fetchAll(2);
    }

    public static function delete_notice($notice_id)
    {
        $notice = self::notice_by_id_single($notice_id);
        unlink('../files/'.$notice['uploaded_file']);

        $sql = "DELETE FROM notice_board WHERE notice_id = ?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array($notice_id));
    }

    public static function delete_announcement($notice_id)
    {
        $notice = self::notice_by_id_single($notice_id);
        unlink('files/'.$notice['uploaded_file']);

        $sql = "DELETE FROM notice_board WHERE notice_id = ?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array($notice_id));
    }

    public static function all_notices()
    {
        $sql = "SELECT * FROM `notice_board` WHERE public = ? ORDER BY notice_id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(1));

        return $stmt->fetchAll(2);
    }

    public static function notice_by_id_single($notice_id)
    {
        $sql = "SELECT * FROM `notice_board` WHERE notice_id";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($notice_id));

        return $stmt->fetch(2);
    }

    public static function all_announcement()
    {
        $sql = "SELECT * FROM `notice_board` WHERE public = ? ORDER BY notice_id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0));

        return $stmt->fetchAll(2);
    }

    public static function update_notice($values,$has_new_file)
    {
        if($has_new_file == true)
        {
            $sql = "UPDATE notice_board SET notice_topic=?,notice_body=?,uploaded_file=?,public=?,batch_name=? WHERE notice_id=?";
        }
        else
        {
            $sql = "UPDATE notice_board SET notice_topic=?,notice_body=?,public=?,batch_name=? WHERE notice_id=?";
        }

        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function update_announcement($values,$has_new_file)
    {
        if($has_new_file == true)
        {
            $sql = "UPDATE notice_board SET notice_topic=?,notice_body=?,uploaded_file=? WHERE notice_id=?";
        }
        else
        {
            $sql = "UPDATE notice_board SET notice_topic=?,notice_body=? WHERE notice_id=?";
        }

        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }
}