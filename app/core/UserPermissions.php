<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/21/18
 * Time: 11:16 PM
 */

namespace user_permissions;


class UserPermissions {

    private static $db;

    public static function db_config($db_config)
    {
        self::$db = $db_config;
    }

    public static function get_user_perms($student_role)
    {
        $sql = "SELECT * FROM `role_control` WHERE student_role=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($student_role));
        return $stmt->fetch(2);
    }

}