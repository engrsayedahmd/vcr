<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/27/18
 * Time: 10:24 PM
 */

namespace students;


class Students {

    private static $db;

    public static function db_config($db_config)
    {
        self::$db = $db_config;
    }

    public static function student_info_by_id($student_id)
    {
        $sql = "SELECT * FROM `students` WHERE student_id =?";
        $stmt=self::$db->prepare($sql);
        $stmt->execute(array($student_id));

        return $stmt->fetch(2);
    }

    public static function cr_all()
    {
        $sql = "SELECT * FROM `students` WHERE student_role =?";
        $stmt=self::$db->prepare($sql);
        $stmt->execute(array('cr'));

        return $stmt->fetchAll(2);
    }

    public static function add_student($values)
    {
        $sql = "INSERT INTO students(student_id,student_name,password,student_role,student_batch,created_on) VALUES(?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function assign_cr($student_id)
    {
        $sql = "UPDATE students SET student_role = ? WHERE student_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array('cr',$student_id));
    }

    public static function students_all()
    {
        $sql = "SELECT * FROM `students` ORDER BY id DESC";
        $stmt=self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }
}