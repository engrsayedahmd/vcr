<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/27/18
 * Time: 10:24 PM
 */

namespace batches;


class Batches {

    private static $db;

    public static function db_config($db_config)
    {
        self::$db = $db_config;
    }

    public static function add_batch($values)
    {
        $sql = "INSERT INTO batches (batch_name,created_on) VALUES (?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function batches_all()
    {
        $sql = "SELECT * FROM `batches` ORDER BY id DESC ";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }
}