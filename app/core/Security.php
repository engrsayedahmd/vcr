<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 5/4/2018
 * Time: 9:21 PM
 */

namespace security;

class Security {

    public static function faculty_access_control()
    {
        if(!isset($_SESSION['faculty']))
        {
            header('Location: login.php');
        }
    }

    public static function student_access_control()
    {
        if(!isset($_SESSION['student']))
        {
            header('Location: login.php');
        }
    }
}