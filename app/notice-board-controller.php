<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 4/21/18
 * Time: 1:13 AM
 */

require_once 'db/db.php';
require_once 'core/NoticeBoard.php';
require_once 'core/Faculty.php';
require_once 'core/Authentication.php';
require_once 'core/Students.php';
require_once 'inc/session.php';

use notice_board\NoticeBoard as nb;
use \faculty\Faculty as faculty;
use \students\Students as std;
use \auth\Authentication as auth;

//  Database Connection
nb::db_config($db);
faculty::db_config($db);
std::db_config($db);
auth::db_config($db);

date_default_timezone_set('Asia/Dhaka');

function file_upload($path)
{
    if(is_uploaded_file($_FILES['file']['tmp_name']))
    {
        if($_FILES['file']['error'] == UPLOAD_ERR_OK)
        {
            $extension = pathinfo(basename($_FILES['file']['name']), PATHINFO_EXTENSION);
            $file_name = basename($_FILES['file']['name'], $extension).mt_rand(0,999999).'.'.$extension;

            if(move_uploaded_file($_FILES['file']['tmp_name'], $path.'/'.$file_name))
            {
                return $file_name;
            }
            else
            {
                return false;
            }
        }
    }
}

function add_notice($user_type)
{
    $posted_by = null;
    $poster_type = null;

    if($user_type == 'faculty')
    {
        $faculty_info = faculty::faculty_info($_SESSION['faculty']);
        $posted_by = $faculty_info['name'];
        $poster_type = 'faculty';
    }
    elseif ($user_type == 'admin')
    {
        $admin_info = auth::admin_info($_SESSION['admin']);
        $posted_by = $admin_info['name'];
        $poster_type = 'admin';
    }

    $public = ($_POST['share_with'] == 'Public') ? 1 : 0;
    $batch_name = ($_POST['share_with'] != 'Public') ? $_POST['share_with'] : null;
    $file_name = file_upload('../files');

    $values = array(
        $_POST['notice_topic'],
        $_POST['notice_body'],
        $file_name,
        $posted_by,
        $poster_type,
        $public,
        $batch_name,
        date("Y-m-d H:i:s")
    );

   return nb::add_notice($values);
}

function add_announcement()
{
    $student_info = std::student_info_by_id(3047);

    $posted_by = null;
    $poster_type = null;

    $posted_by = $student_info['student_name'];
    $poster_type = 'cr';

    $public = 0;
    $batch_name = $student_info['student_batch'];
    $file_name = file_upload('files');

    $values = array(
        $_POST['notice_topic'],
        $_POST['notice_body'],
        $file_name,
        $posted_by,
        $poster_type,
        $public,
        $batch_name,
        date("Y-m-d H:i:s")
    );

    return nb::add_notice($values);
}

function faculty_notices()
{
    $faculty_info = faculty::faculty_info($_SESSION['faculty']);
    $faculty_name = $faculty_info['name'];

    return nb::faculty_notices($faculty_name);
}

function admin_notices()
{
    return nb::admin_notices();
}

function student_notices()
{
    $student_info = std::student_info_by_id($_SESSION['student']);
    $student_batch = $student_info['student_batch'];

    return nb::student_notices($student_batch);
}

function delete_notice($notice_id)
{
    return nb::delete_notice($notice_id);
}

function delete_announcement($notice_id)
{
    return nb::delete_announcement($notice_id);
}

function all_notices()
{
    return nb::all_notices();
}

function all_announcement()
{
    return nb::all_announcement();
}

function update_notice()
{
    $file_name = null;
    $has_new_file = false;

    $notice_topic = $_POST['notice_topic'];
    $notice_body = $_POST['notice_body'];
    $public = ($_POST['share_with'] == 'Public') ? 1 : 0;
    $batch_name = ($_POST['share_with'] != 'Public') ? $_POST['share_with'] : null;
    $notice_id = $_POST['notice_id'];

    if(!empty($_FILES['file']['size']))
    {
        $file_name = file_upload('../files');
        $has_new_file = true;

        $values = array(
            $notice_topic,
            $notice_body,
            $file_name,
            $public,
            $batch_name,
            $notice_id
        );
    }
    else
    {
        $values = array(
            $notice_topic,
            $notice_body,
            $public,
            $batch_name,
            $notice_id
        );
    }

    return nb::update_notice($values,$has_new_file);
}

function update_announcement()
{
    $file_name = null;
    $has_new_file = false;

    $notice_topic = $_POST['notice_topic'];
    $notice_body = $_POST['notice_body'];
    $notice_id = $_POST['notice_id'];

    if(!empty($_FILES['file']['size']))
    {
        $file_name = file_upload('files');
        $has_new_file = true;

        $values = array(
            $notice_topic,
            $notice_body,
            $file_name,
            $notice_id
        );
    }
    else
    {
        $values = array(
            $notice_topic,
            $notice_body,
            $notice_id
        );
    }

    return nb::update_announcement($values,$has_new_file);
}