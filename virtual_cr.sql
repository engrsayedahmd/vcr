-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2018 at 05:09 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtual_cr`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `name`, `password`, `created_on`) VALUES
(2, 'admin@demo.com', 'Alex', 'admin', '2018-04-29');

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` int(11) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `batch_name`, `created_on`) VALUES
(1, 'MIT15', '2018-04-27'),
(2, 'EEE32', '2018-04-27'),
(3, 'CSE46', '2018-04-27'),
(4, 'MIT25', '2018-04-28');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `initial` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `name`, `initial`, `phone`, `email`, `password`, `created_on`) VALUES
(1, 'Fahad Faysal', 'FF', '01771224089', 'fahad@demo.com', 'faculty', '2018-05-04'),
(2, 'Hasanuzzaman', 'HZ', '01771224089', 'hassan.zamman@demo.com', 'faculty', '2018-05-05');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_batches`
--

CREATE TABLE `faculty_batches` (
  `id` int(11) NOT NULL,
  `faculty_id` varchar(255) NOT NULL,
  `batch_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_batches`
--

INSERT INTO `faculty_batches` (`id`, `faculty_id`, `batch_name`) VALUES
(1, '1', 'MIT15'),
(2, '1', 'MIT25'),
(3, '2', 'MIT25'),
(4, '2', 'MIT15');

-- --------------------------------------------------------

--
-- Table structure for table `notice_board`
--

CREATE TABLE `notice_board` (
  `notice_id` int(11) NOT NULL,
  `notice_topic` varchar(255) NOT NULL,
  `notice_body` longtext NOT NULL,
  `uploaded_file` text,
  `posted_by` varchar(255) NOT NULL,
  `poster_type` varchar(255) NOT NULL,
  `public` int(10) NOT NULL,
  `batch_name` varchar(255) DEFAULT NULL,
  `posted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice_board`
--

INSERT INTO `notice_board` (`notice_id`, `notice_topic`, `notice_body`, `uploaded_file`, `posted_by`, `poster_type`, `public`, `batch_name`, `posted_on`) VALUES
(2, 'Off Day', 'Tomorrow is off day', NULL, 'Fahad Faysal', 'faculty', 0, 'MIT25', '2018-05-05 10:13:35'),
(3, 'AI', 'Exam will start next monday', NULL, 'Fahad Faysal', 'faculty', 0, 'MIT15', '2018-05-05 10:32:40'),
(7, 'OS', 'Tomorrow is class test', NULL, 'Md. Sayed Ahammed', 'cr', 0, 'MIT15', '2018-05-05 01:42:12'),
(8, 'Topic', 'Admin Notice', 'IMG_20180112_162706.218970.jpg', 'Alex', 'admin', 1, NULL, '2018-05-05 08:25:03'),
(9, 'Admin', 'Admin', 'IMG_20180112_162510.166465.jpg', 'Alex', 'admin', 1, NULL, '2018-05-05 08:26:38');

-- --------------------------------------------------------

--
-- Table structure for table `role_control`
--

CREATE TABLE `role_control` (
  `id` int(11) NOT NULL,
  `student_role` varchar(255) NOT NULL,
  `crud_notice` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_control`
--

INSERT INTO `role_control` (`id`, `student_role`, `crud_notice`) VALUES
(1, 'cr', '1'),
(2, 'student', '0');

-- --------------------------------------------------------

--
-- Table structure for table `routine`
--

CREATE TABLE `routine` (
  `id` int(11) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `f_initial` varchar(255) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `creation_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `routine`
--

INSERT INTO `routine` (`id`, `batch_name`, `day`, `subject`, `code`, `f_initial`, `start_time`, `end_time`, `creation_date`) VALUES
(1, 'MIT15', 'Sat', 'AI', '102', 'FF', '11:00:00', '12:30:00', '2018-04-25'),
(2, 'MIT15', 'Sat', 'OS', '104', 'FF', '08:30:00', '10:00:00', '2018-04-25'),
(3, 'MIT15', 'Sun', 'OS', '104', 'FF', '13:00:00', '14:30:00', '2018-04-25'),
(4, 'MIT25', 'Sun', 'OS', '104', 'FF', '01:01:00', '14:02:00', '2018-04-25'),
(5, 'MIT25', 'Sat', 'AI', '110', 'MHZ', '01:01:00', '14:02:00', '2018-04-25'),
(6, 'MIT25', 'Sun', 'OOP', '404', 'FF', '15:00:00', '16:30:00', '2018-04-26'),
(7, 'MIT25', 'Sun', 'Algo', '450', 'FF', '10:00:00', '11:30:00', '2018-04-29'),
(8, 'MIT25', 'Sun', 'General History', '450', 'HZ', '10:00:00', '11:30:00', '2018-05-05');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `student_role` varchar(255) NOT NULL,
  `student_batch` varchar(255) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_id`, `student_name`, `password`, `student_role`, `student_batch`, `created_on`) VALUES
(10, '3047', 'Md. Sayed Ahammed', 'student', 'cr', 'MIT15', '2018-05-04'),
(11, '3000', 'Abuhena Rony', 'student', 'student', 'MIT25', '2018-05-05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_batches`
--
ALTER TABLE `faculty_batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice_board`
--
ALTER TABLE `notice_board`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `role_control`
--
ALTER TABLE `role_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routine`
--
ALTER TABLE `routine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `batch` (`batch_name`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faculty_batches`
--
ALTER TABLE `faculty_batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notice_board`
--
ALTER TABLE `notice_board`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role_control`
--
ALTER TABLE `role_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `routine`
--
ALTER TABLE `routine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
