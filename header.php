<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>VCR</title>



    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">


</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->
    <div class="pre-loader-area">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header class="header-area">
        <!--    [ Strat Logo Area]-->
        <nav class="navbar navbar-light navbar-expand-md justify-content-between">
            <a href="home.php" class="navbar-brand">VIRTUAL CR</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
            <span class="navbar-toggler-icon"></span>
        </button>
            <div class="navbar-collapse collapse justify-content-between" id="collapsingNavbar2">
                <div>
                    <!--placeholder to evenly space flexbox items and center links-->
                </div>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="home.php">Home</a>
                    </li>
                    <?php if($user_perms['crud_notice']): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="announcement.php">Announcement</a>
                    </li>
                    <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Hi, <?= $_SESSION['student_name']?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Profile</a>
                            <a class="dropdown-item" href="logout.php">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
