<?php

require_once 'app/routine-controller.php';
require_once 'app/notice-board-controller.php';
require_once 'app/user-permissions-controller.php';
require_once 'app/students-controller.php';
require_once 'app/security-controller.php';

student_access_control();

$student_info = student_info_by_id($_SESSION['student']);

$student_routine = student_routine($student_info['student_batch']);
$all_notices = all_notices();
$all_announcement = all_announcement();
$user_perms = get_user_perms($student_info['student_role']);

?>

<?php require('header.php'); ?>
<!--    [ Strat Section Area]-->
<section id="notice">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="page-title">
                    <h5>HOME - NOTICE & ROUTINE </h5>
                </div>
            </div>
        </div>
        <div class="notice-content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="notice-board">
                        <div class="card">
                            <div class="card-header">
                                <p>Notice & Announcement</p>
                            </div>
                            <div class="card-body">
                                <!--tabs for notice & ann-->
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Announcement</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Notice</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="all-notices">

                                            <?php foreach ($all_announcement as $announcement): ?>
                                                <div class="single-notice">
                                                    <div class="notice-date">
                                                        <p><?= $announcement['notice_topic'] ?></p>
                                                    </div>
                                                    <div class="note-poster">
                                                        <h5><?= date("F j, Y, g:i a", strtotime($announcement['posted_on'])); ?> <span>*<?= $announcement['poster_type']; ?>* - <?= $announcement['posted_by'].' - ('.$announcement['batch_name'].')' ?></span></h5>
                                                        <p><?= $announcement['notice_body'] ?></p>

                                                        <?php if(!empty($announcement['uploaded_file'])): ?>
                                                            <p>
                                                                File :<a href="<?= '../files/'.$announcement['uploaded_file']?>" class="btn btn-default"><?= $announcement['uploaded_file']?></a>
                                                            </p>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <hr>
                                            <?php endforeach; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                        <div class="all-notices">

                                            <?php foreach ($all_notices as $notice): ?>
                                                <div class="single-notice">
                                                    <div class="notice-date">
                                                        <p><?= $notice['notice_topic'] ?></p>
                                                    </div>
                                                    <div class="note-poster">
                                                        <h5><?= date("F j, Y, g:i a", strtotime($notice['posted_on'])); ?> <span>*<?= $notice['poster_type']; ?>* - <?= $notice['posted_by'] ?></span></h5>
                                                        <p><?= $notice['notice_body'] ?></p>

                                                        <?php if(!empty($notice['uploaded_file'])): ?>
                                                            <p>
                                                                File: <a href="<?= '../files/'.$notice['uploaded_file']?>" class="btn btn-default"><?= $notice['uploaded_file']?></a>
                                                            </p>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <hr>
                                            <?php endforeach; ?>

                                        </div>
                                    </div>
                                </div>
                                <!--tabs for notice & ann-->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="notice-board">
                        <div class="card">
                            <div class="card-header">
                                <p>Routine</p>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <tr>
                                        <th>Day</th>
                                        <th>Subject</th>
                                        <th>Code</th>
                                        <th>Teacher</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                    </tr>
                                    <?php foreach ($student_routine as $routine): ?>
                                        <tr>
                                            <td><?= $routine['day'] ?></td>
                                            <td><?= $routine['subject'] ?></td>
                                            <td><?= $routine['code'] ?></td>
                                            <td><?= $routine['f_initial'] ?></td>
                                            <td><?= $routine['start_time'] ?></td>
                                            <td><?= $routine['end_time'] ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
